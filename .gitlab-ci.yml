# Change pip's cache directory to be inside the project directory since we can only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

stages:
  - install
  - quality-check
  - unit-test
  - githooks-test
  - publish

default:
  tags:
    - ionos-runner
  before_script:
    - python3 --version
    - python3 -m pip --version
    - which python3
    - python3 -m pip cache dir
    - python3 -m venv .venv
    - source .venv/bin/activate
  cache:
    key: install-cache
    paths:
      - .cache/pip/
      - .venv/
  artifacts:
    expire_in: 1 week

deploy-install-job:
  stage: install
  script:
    - echo ___ Installing as Python package ___
    - python3 -m pip install .
    - python3 -m projecteuler --version
    - projecteuler --version

dev-install-job:
  stage: install
  script:
    - echo ___ Installing as Python package ___
    - python3 -m pip install --editable '.[dev]'
    - python3 -m ruff --version
    - python3 -m mypy --version
    - python3 -m pytest --version
    - python3 -m coverage --version
    - python3 -m pdoc --version
    - python3 -m projecteuler --version
    - projecteuler --version

format-job:
  stage: quality-check
  needs:
    - job: dev-install-job
      artifacts: false
  script:
    - echo ___ Formatting code ___
    - python3 -m ruff --version
    - python3 -m ruff format --config pyproject.toml
    - '(git diff --name-only) >_git_diff.txt'
    - '[ -s _git_diff.txt ] && exit 1 || exit 0'
  allow_failure: true
  artifacts:
    when: on_failure
    paths:
      - _git_diff.txt

lint-job:
  stage: quality-check
  needs:
    - job: dev-install-job
      artifacts: false
  script:
    - echo ___ Linting code ___
    - python3 -m ruff --version
    - python3 -m ruff check --config pyproject.toml
    - python3 -m ruff check --config pyproject.toml --output-format junit --output-file _lint.xml
    - python3 -m ruff check --config pyproject.toml --output-format gitlab --output-file _lint.json
  allow_failure: true
  artifacts:
    paths:
      - _lint.xml
      - _lint.json
    reports:
      junit: _lint.xml
      codequality: _lint.json

typing-job:
  stage: quality-check
  needs:
    - job: dev-install-job
      artifacts: false
  script:
    - echo ___ Type checking code ___
    - python3 -m mypy --version
    - python3 -m mypy --config-file pyproject.toml --junit-xml _typing.xml
  allow_failure: true
  artifacts:
    paths:
      - _typing.xml
    reports:
      junit: _typing.xml

test-job:
  stage: unit-test
  needs:
    - job: dev-install-job
      artifacts: false
  script:
    - echo ___ Running tests ___
    - python3 -m pytest --version
    - python3 -m pytest --junit-xml _tests.xml
  artifacts:
    paths:
      - _tests.xml
    reports:
      junit: _tests.xml

coverage-job:
  stage: unit-test
  needs:
    - job: dev-install-job
      artifacts: false
  script:
    - echo ___ Measuring code coverage ___
    - python3 -m pytest --version
    - python3 -m coverage --version
    - python3 -m coverage run -m pytest
    - python3 -m coverage report
    - python3 -m coverage xml -o _coverage.xml
    - python3 -m coverage html --directory _html/coverage
  coverage: '/TOTAL.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    paths:
      - _coverage.xml
      - _html/coverage/
    reports:
      coverage_report:
        coverage_format: cobertura
        path: _coverage.xml

pre-commit-job:
  stage: githooks-test
  needs:
    - job: dev-install-job
      artifacts: false
  script:
    - echo ___ Running pre-commit script ___
    - .githooks/pre-commit

pages:
  stage: publish
  needs:
    - job: dev-install-job
      artifacts: false
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
  script:
    - echo "___ Building documentation and deploying website <$CI_PAGES_URL> ___"
    - python3 -m pdoc --version
    - python3 -m pdoc --html --output-dir _html/docs projecteuler --force
  artifacts:
    paths:
      - _html/docs/projecteuler/
  publish: _html/docs/projecteuler
