import importlib.metadata
import pathlib

import projecteuler


class TestInit:
    def test_metadata(self):
        for string in (
            projecteuler.__all__[0],
            projecteuler.__author__,
            projecteuler.__doc__,
            projecteuler.__file__,
            projecteuler.__name__,
            projecteuler.__package__,
            projecteuler.__path__[0],
            projecteuler.__version__,
        ):
            assert isinstance(string, str)
            assert len(string) > 0

        metadata = importlib.metadata.metadata("projecteuler")

        assert projecteuler.__author__ == metadata.get("Author")
        assert projecteuler.__doc__.strip().splitlines()[0] == metadata.get("Summary")
        assert projecteuler.__package__ == metadata.get("Name").casefold()
        assert projecteuler.__version__ == metadata.get("Version")

    def test_version(self, rootpath: pathlib.Path):
        with pathlib.Path(rootpath).joinpath("VERSION").open("r") as version_file:
            version = version_file.read().strip()

        assert projecteuler.__version__ == version

    def test_typed(self):
        assert pathlib.Path(projecteuler.__path__[0]).joinpath("py.typed").is_file()

    def test_imports(self):
        for module_str in projecteuler.__all__:
            assert hasattr(projecteuler, module_str)

        for module_str in projecteuler.problems.__all__:
            assert hasattr(projecteuler.problems, module_str)

        for module_str in projecteuler.problems.p763.__all__:
            assert hasattr(projecteuler.problems.p763, module_str)
