import argparse

import pytest

import projecteuler


@pytest.mark.parametrize("async_", [False, True])
@pytest.mark.parametrize("images", [False, True])
@pytest.mark.parametrize(
    "divisions, dimensions, result",
    [
        # Corner cases
        (0, 0, 0),
        (5, 0, 0),
        (0, 5, 0),
        (5, 1, 1),
        (1, 5, 1),
        # 2D space grid
        (5, 2, 20),
        # 3D space grid
        (5, 3, 99),
    ],
)
class TestP762:
    def test_main(self, divisions, dimensions, result, images, async_):  # noqa: PLR0913
        args = argparse.Namespace(divisions=divisions, dimensions=dimensions, images=images, async_=async_)
        assert projecteuler.problems.p762.main(args) == result


class TestAmoebasVariant:
    @pytest.mark.parametrize(
        ("divisions", "dimensions", "result"),
        [
            # 2D space grid
            (10, 2, (1, 2, 4, 9, 20, 46, 105, 243, 561, 1301)),
            # 3D space grid
            (10, 3, (1, 3, 9, 30, 99, 336, 1134, 3855, 13086, 44499)),
            # nD space grid
            (6, 4, (1, 4, 16, 70, 304, 1348)),
            (6, 5, (1, 5, 25, 135, 725, 3955)),
            (6, 6, (1, 6, 36, 231, 1476, 9546)),
        ],
    )
    def test_recurse(self, divisions, dimensions, result):
        memory = projecteuler.problems.p763.Memory(divisions)
        amoebas = projecteuler.problems.p762.AmoebasVariant(memory)

        initial_arrangement: set[tuple[int, ...]] = {(0,) * dimensions}

        amoebas.recurse(initial_arrangement, 0)

        assert amoebas.n_arrangements == result
