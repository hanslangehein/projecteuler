import argparse

import pytest

import projecteuler

pytestmark = pytest.mark.parametrize("bound, result", [(10, 23), (1000, 233168)])


class TestP1:
    def test_main(self, bound, result):
        args = argparse.Namespace(bound=bound)
        assert projecteuler.problems.p1.main(args) == result
