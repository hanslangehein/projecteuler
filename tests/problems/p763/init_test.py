import argparse

import pytest

import projecteuler


@pytest.mark.parametrize("async_", [False, True])
@pytest.mark.parametrize("images", [False, True])
@pytest.mark.parametrize(
    "divisions, dimensions, result",
    [
        # Corner cases
        (0, 0, 0),
        (5, 0, 0),
        (0, 5, 0),
        (5, 1, 1),
        (1, 5, 1),
        # 2D space grid
        (5, 2, 20),
        # 3D space grid
        (5, 3, 99),
    ],
)
class TestInit:
    def test_main(self, divisions, dimensions, result, images, async_):  # noqa: PLR0913
        args = argparse.Namespace(divisions=divisions, dimensions=dimensions, images=images, async_=async_)
        assert projecteuler.problems.p763.main(args) == result
