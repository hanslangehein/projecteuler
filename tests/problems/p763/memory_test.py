import asyncio

import PIL.Image
import PIL.ImageChops
import pytest

import projecteuler

from . import amoebas_test


class ImageMemoryWithException(projecteuler.problems.p763.ImageMemory):
    def _save(self, arrangement, depth):
        frames = list(self.load_frames(self.filepath(depth)))
        new_frame = self.create_frame(arrangement, depth)

        # If all pixels are equal, the arrangement already exists in (any) one of the existing frames
        if any(all(pixel == 0 for pixel in PIL.ImageChops.difference(frame, new_frame).getdata()) for frame in frames):
            msg = "Arrangement already exists"
            raise Warning(msg)

        frames.append(new_frame)
        self.save_frames(frames, self.filepath(depth))

        # If the amount of frames has changed, a frame has been lost
        old_frames = tuple(self.load_frames(self.filepath(depth)))
        if len(old_frames) != len(frames):
            msg = "Arrangement could not be saved"
            raise MemoryError(msg)

        # If any pixel is unequal, a change within one frame has occured after loading
        if any(
            any(pixel != 0 for pixel in PIL.ImageChops.difference(frame, old_frame).getdata())
            for frame, old_frame in zip(frames, old_frames, strict=False)
        ):
            msg = "Arrangement could not be saved without loss of precision"
            raise AssertionError(msg)


class ImageMemoryWithFrameLoss(ImageMemoryWithException):
    @classmethod
    def save_frames(cls, imgs, filepath):
        img = imgs[0]
        img.save(filepath, append_images=imgs, save_all=False, optimize=False)


class TestMemory:
    @pytest.mark.parametrize("async_", [False, True])
    def test_image_memory(self, async_):
        divisions = 7
        dimensions = 2
        result = (1, 2, 4, 9, 20, 46, 105)

        memory = projecteuler.problems.p763.ImageMemory(divisions)
        amoebas = amoebas_test.AmoebasWithoutPermute(memory)

        initial_arrangement: set[tuple[int, ...]] = {(0,) * dimensions}

        if async_:
            asyncio.run(amoebas.recurse_async(initial_arrangement, 0))
        else:
            amoebas.recurse(initial_arrangement, 0)

        assert amoebas.n_arrangements == result

        assert memory.DIRNAME.is_dir()

        for depth in range(1, divisions + 1):
            filepath = memory.filepath(depth)
            filepath.is_file()
            frames = tuple(memory.load_frames(filepath))
            assert len(frames) == result[depth - 1] + 1

    @pytest.mark.parametrize(
        ("divisions", "dimensions", "result"),
        [
            # Corner cases
            (5, 0, (1, 0, 0, 0, 0)),
            (5, 1, (1, 1, 1, 1, 1)),
            # 2D space grid
            (5, 2, (1, 2, 4, 9, 20)),
            # 3D space grid
            (5, 3, (1, 3, 9, 30, 99)),
        ],
    )
    def test_image_memory_with_exception(self, divisions, dimensions, result):
        memory = ImageMemoryWithException(divisions)
        amoebas = projecteuler.problems.p763.Amoebas(memory)

        initial_arrangement: set[tuple[int, ...]] = {(0,) * dimensions}

        if dimensions == 0:
            with pytest.raises(Warning):
                amoebas.recurse(initial_arrangement, 0)

            assert amoebas.n_arrangements == result

        elif dimensions in (1, 2):
            amoebas.recurse(initial_arrangement, 0)

            assert amoebas.n_arrangements == result

        else:
            with pytest.raises(Warning):
                amoebas.recurse(initial_arrangement, 0)

    def test_image_memory_with_frame_loss(self):
        divisions, dimensions = 5, 2

        memory = ImageMemoryWithFrameLoss(divisions)
        amoebas = projecteuler.problems.p763.Amoebas(memory)

        initial_arrangement: set[tuple[int, ...]] = {(0,) * dimensions}

        with pytest.raises(MemoryError):
            amoebas.recurse(initial_arrangement, 0)
