import itertools

import pytest

import projecteuler


class AmoebasWithoutPermute(projecteuler.problems.p763.Amoebas):
    @classmethod
    def permute(cls, arrangement):
        yield from (arrangement,)


class TestAmoebas:
    def test_evolve(self):
        amoeba = ()
        amoebas = tuple(projecteuler.problems.p763.Amoebas.evolve(amoeba))
        assert len(amoebas) == 0

        amoeba = (10,)
        amoebas = tuple(projecteuler.problems.p763.Amoebas.evolve(amoeba))
        assert len(amoebas) == 1
        assert (11,) in amoebas

        amoeba = (2, 6)
        amoebas = tuple(projecteuler.problems.p763.Amoebas.evolve(amoeba))
        assert len(amoebas) == 2
        assert (3, 6) in amoebas
        assert (2, 7) in amoebas

        amoeba = (1, 3, 5)
        amoebas = tuple(projecteuler.problems.p763.Amoebas.evolve(amoeba))
        assert len(amoebas) == 3
        assert (2, 3, 5) in amoebas
        assert (1, 4, 5) in amoebas
        assert (1, 3, 6) in amoebas

        amoeba = (0,) * 4
        amoebas = tuple(projecteuler.problems.p763.Amoebas.evolve(amoeba))
        assert len(amoebas) == 4
        assert (1, 0, 0, 0) in amoebas
        assert (0, 1, 0, 0) in amoebas
        assert (0, 0, 1, 0) in amoebas
        assert (0, 0, 0, 1) in amoebas

    def test_permute(self):
        arrangement: set[tuple[int, ...]]

        arrangements = tuple(projecteuler.problems.p763.Amoebas.permute(set()))
        assert len(arrangements) == 1
        for arrangement in arrangements:
            assert len(arrangement) == 0

        amoeba0 = (10, 0)
        amoeba1 = (2, 3)
        amoeba2 = (0, 0)
        arrangement = {amoeba0, amoeba1, amoeba2}
        arrangements = tuple(projecteuler.problems.p763.Amoebas.permute(arrangement))
        assert len(arrangements) == 2
        for arrangement in arrangements:
            assert len(arrangement) == 3
        for permuted_amoeba, arrangement in zip(itertools.permutations(amoeba0), arrangements, strict=True):
            assert permuted_amoeba in arrangement
        for permuted_amoeba, arrangement in zip(itertools.permutations(amoeba1), arrangements, strict=True):
            assert permuted_amoeba in arrangement
        for permuted_amoeba, arrangement in zip(itertools.permutations(amoeba2), arrangements, strict=True):
            assert permuted_amoeba in arrangement

        amoeba0 = (10, 0, 2)
        arrangement = {amoeba0}
        arrangements = tuple(projecteuler.problems.p763.Amoebas.permute(arrangement))
        assert len(arrangements) == 6
        for arrangement in arrangements:
            assert len(arrangement) == 1
        for permuted_amoeba, arrangement in zip(itertools.permutations(amoeba0), arrangements, strict=True):
            assert permuted_amoeba in arrangement

    @pytest.mark.parametrize("amoebas_class", [projecteuler.problems.p763.Amoebas, AmoebasWithoutPermute])
    @pytest.mark.parametrize(
        ("divisions", "dimensions", "result"),
        [
            # 2D space grid
            (10, 2, (1, 2, 4, 9, 20, 46, 105, 243, 561, 1301)),
            # 3D space grid
            (10, 3, (1, 3, 9, 30, 99, 336, 1134, 3855, 13086, 44499)),
            # nD space grid
            (6, 4, (1, 4, 16, 70, 304, 1348)),
            (6, 5, (1, 5, 25, 135, 725, 3955)),
            (6, 6, (1, 6, 36, 231, 1476, 9546)),
        ],
    )
    def test_recurse(self, divisions, dimensions, result, amoebas_class):
        memory = projecteuler.problems.p763.Memory(divisions)
        amoebas = amoebas_class(memory)

        initial_arrangement: set[tuple[int, ...]] = {(0,) * dimensions}

        amoebas.recurse(initial_arrangement, 0)

        assert amoebas.n_arrangements == result
