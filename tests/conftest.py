import pathlib

import pytest


@pytest.fixture(scope="package")
def rootpath(pytestconfig: pytest.Config) -> pathlib.Path:
    rootpath_ = pathlib.Path(pytestconfig.rootpath)
    assert rootpath_.is_dir()
    return rootpath_
