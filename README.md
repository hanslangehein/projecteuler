ProjectEuler: Solutions to Project Euler problems.
===

## Table of Contents

- [ProjectEuler: Solutions to Project Euler problems.](#projecteuler-solutions-to-project-euler-problems)
    - [Table of Contents](#table-of-contents)
    - [Background](#background)
    - [Installation](#installation)
        - [Deployment mode](#deployment-mode)
        - [Development mode](#development-mode)
    - [Usage](#usage)
        - [Overview](#overview)
        - [Help and usage messages](#help-and-usage-messages)
    - [Project documentation](#project-documentation)
    - [License](#license)
    - [Contact](#contact)

## Background

See <https://projecteuler.net/about> to learn more about Project Euler.

## Installation

[reqs]: https://packaging.python.org/en/latest/tutorials/installing-packages/#requirements-for-installing-packages
[inst]: https://pip.pypa.io/en/stable/cli/pip_install/#examples
[venv]: https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#create-and-use-virtual-environments

### Deployment mode

1. Ensure you can run Python and pip from the command line (see [Requirements for Installing Packages][reqs]):
    ```bash
    python3 --version
    python3 -m pip --version
    ```
1. Install this project from VCS (see [Examples][inst]):
    ```bash
    python3 -m pip install 'git+https://gitlab.com/hanslangehein/projecteuler.git'
    ```

### Development mode

1. Ensure you can run Python and pip from the command line (see [Requirements for Installing Packages][reqs]):
    ```bash
    python3 --version
    python3 -m pip --version
    ```
1. It is recommended to use virtual environments, which isolate the installed packages from the system packages (see [Create and Use Virtual Environments][venv]):
    1. This requires an extra installation step in some Linux distributions:
        ```bash
        apt install python3-venv
        ```
    1. Create the environment:
        ```bash
        python3 -m venv .venv
        ```
    1. Activate the environment:
        ```bash
        source .venv/bin/activate
        ```
    1. To confirm activation, check the location of your Python interpreter:
        ```bash
        which python3
        ```
1. Perform an editable installation using the `dev` identifier for extra dependencies (see [Examples][inst]):
    - Either by installing from local project:
        1. Clone project and navigate to its root directory
        1. Install project:
            ```bash
            python3 -m pip install --editable '.[dev]'
            ```
    - Or by performing an editable VCS install:
        1. Install project:
            ```bash
            python3 -m pip install --editable 'git+https://gitlab.com/hanslangehein/projecteuler.git#egg=ProjectEuler[dev]'
            ```
        1. To show clone location, list installed Python packages:
            ```bash
            python3 -m pip list
            ```
1. To enable this project's hooks for automated execution by git, change git's hooks directory to [.githooks](.githooks):
    ```bash
    git config --local core.hooksPath .githooks
    ```

## Usage

### Overview

To launch **ProjectEuler** scripts you have the following options:
- Via command-line wrapper:
    ```bash
    projecteuler
    ```
- Via invocation as Python module:
    ```bash
    python3 -m projecteuler
    ```

### Help and usage messages

Pass `--help` as a command-line argument to display help and usage messages.

## Project documentation

See <https://hanslangehein.gitlab.io/projecteuler> to get an overview of **ProjectEuler**'s module hierarchy.

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](/LICENSE) for more details.

## Contact

If you are interested in contributing, please feel free to contact me by email or submit a merge request.
