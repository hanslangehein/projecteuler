"""
ProjectEuler: Solutions to Project Euler problems.

See <https://projecteuler.net/about> to learn more.

Copyright (C) 2024  Hans Langehein

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import projecteuler

projecteuler.main()
