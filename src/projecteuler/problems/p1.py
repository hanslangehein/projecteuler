"""
P1: Multiples of 3 or 5.

See <https://projecteuler.net/problem=1> for problem statement.

Copyright (C) 2024  Hans Langehein

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import logging

import projecteuler

logger = logging.getLogger(__name__)


def main(args: argparse.Namespace) -> int:
    """"""

    return sum(ii for ii in range(args.bound) if ii % 3 == 0 or ii % 5 == 0)


def parser_setup(sub_parser: argparse.ArgumentParser) -> None:
    """"""

    sub_parser.description = __doc__
    sub_parser.formatter_class = argparse.RawDescriptionHelpFormatter
    sub_parser.add_argument("bound", type=int, help="upper bound (open interval)")
    sub_parser.set_defaults(main_function=main)


if __name__ == "__main__":
    sub_parser = argparse.ArgumentParser(parents=(projecteuler.get_parent_parser(),))
    parser_setup(sub_parser)

    args = sub_parser.parse_args()
    projecteuler.logging_setup(args)
    result = args.main_function(args)

    logger.info("Result: %s", result)
