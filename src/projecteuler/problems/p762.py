"""
P762: Amoebas in a 2D Grid.

See <https://projecteuler.net/problem=762> for problem statement.

Copyright (C) 2024  Hans Langehein

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import asyncio
import logging
import typing

import projecteuler
from projecteuler.problems.p763.amoebas import Amoebas
from projecteuler.problems.p763.memory import ImageMemory, Memory

logger = logging.getLogger(__name__)


def main(args: argparse.Namespace) -> int:
    """"""

    memory = ImageMemoryVariant(args.divisions) if args.images else Memory(args.divisions)
    amoebas = AmoebasVariant(memory)

    initial_arrangement: set[tuple[int, ...]] = {(0,) * args.dimensions}

    logger.info("Recursion started")

    if args.async_:
        asyncio.run(amoebas.recurse_async(initial_arrangement, 0))
    else:
        amoebas.recurse(initial_arrangement, 0)

    logger.info("Done")

    n_arrangements = amoebas.n_arrangements
    result = n_arrangements[-1] if args.divisions else 0

    logger.info("Number of different possible arrangements for each division in a %sD space grid:", args.dimensions)
    logger.info(n_arrangements)
    logger.info(
        "Number of different possible arrangements after %s divisions in a %sD space grid: %s",
        args.divisions,
        args.dimensions,
        result,
    )

    return result


class AmoebasVariant(Amoebas):
    """"""

    @classmethod
    def evolve(cls, amoeba: tuple[int, ...]) -> set[tuple[int, ...]]:
        """
        Given one amoeba (*n*D coordinate), create *n* child amoebas, where *n* is the number of dimensions.

        In a 2D space grid, this method is equivalent to:

        ```
        x, y = amoeba

        return {(x + 1, y), (x + 1, (y + 1) % 4)}
        ```

        In a 3D space grid, this method is equivalent to:

        ```
        x, y, z = amoeba

        return {(x + 1, y, z), (x + 1, (y + 1) % 4, z), (x + 1, (y + 1) % 4, (z + 1) % 4)}
        ```
        """

        n_dims = len(amoeba)

        child_amoebas = set()
        child_amoeba = list(amoeba)
        for axis in range(n_dims):
            child_amoeba[axis] += 1
            if axis > 0:
                child_amoeba[axis] = child_amoeba[axis] % 4
            child_amoebas.add(tuple(child_amoeba))

        return child_amoebas

    @classmethod
    def permute(cls, arrangement: set[tuple[int, ...]]) -> typing.Iterator[set[tuple[int, ...]]]:
        """"""
        yield from (arrangement,)


class ImageMemoryVariant(ImageMemory):
    """"""

    @classmethod
    def img_size(cls, depth: int) -> tuple[int, int]:
        """Size of an image as 2-tuple, containing width and height in pixels."""

        return (depth + 1, min(depth + 1, 4))


def parser_setup(sub_parser: argparse.ArgumentParser) -> None:
    """"""

    sub_parser.description = __doc__
    sub_parser.formatter_class = argparse.RawDescriptionHelpFormatter
    sub_parser.add_argument("divisions", type=projecteuler.unsigned_int, help="maximum number of divisions allowed")
    sub_parser.add_argument(
        "dimensions", nargs="?", default=2, type=projecteuler.unsigned_int, help="dimensionality of the space grid"
    )
    sub_parser.add_argument("-i", "--images", action="store_true", help="store amoebas colony to disc as images")
    sub_parser.add_argument("-a", "--async", dest="async_", action="store_true", help="run asynchronously")
    sub_parser.set_defaults(main_function=main)


if __name__ == "__main__":
    sub_parser = argparse.ArgumentParser(parents=(projecteuler.get_parent_parser(),))
    parser_setup(sub_parser)

    args = sub_parser.parse_args()
    projecteuler.logging_setup(args)
    result = args.main_function(args)

    logger.info("Result: %s", result)
