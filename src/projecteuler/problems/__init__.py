"""
Problems: Solutions to individual problems.

Copyright (C) 2024  Hans Langehein

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from projecteuler.problems import p1, p762, p763

__all__ = ["p1", "p762", "p763"]
