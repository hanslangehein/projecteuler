"""
P763: Amoebas in a 3D Grid.

See <https://projecteuler.net/problem=763> for problem statement.

Copyright (C) 2024  Hans Langehein

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from projecteuler.problems.p763.amoebas import Amoebas
from projecteuler.problems.p763.memory import ImageMemory, Memory

__all__ = ["Amoebas", "Memory", "ImageMemory"]

import argparse
import asyncio
import logging

import projecteuler

logger = logging.getLogger(__package__)


def main(args: argparse.Namespace) -> int:
    """"""

    memory = ImageMemory(args.divisions) if args.images else Memory(args.divisions)
    amoebas = Amoebas(memory)

    initial_arrangement: set[tuple[int, ...]] = {(0,) * args.dimensions}

    logger.info("Recursion started")

    if args.async_:
        asyncio.run(amoebas.recurse_async(initial_arrangement, 0))
    else:
        amoebas.recurse(initial_arrangement, 0)

    logger.info("Done")

    n_arrangements = amoebas.n_arrangements
    result = n_arrangements[-1] if args.divisions else 0

    logger.info("Number of different possible arrangements for each division in a %sD space grid:", args.dimensions)
    logger.info(n_arrangements)
    logger.info(
        "Number of different possible arrangements after %s divisions in a %sD space grid: %s",
        args.divisions,
        args.dimensions,
        result,
    )

    return result


def parser_setup(sub_parser: argparse.ArgumentParser) -> None:
    """"""

    sub_parser.description = __doc__
    sub_parser.formatter_class = argparse.RawDescriptionHelpFormatter
    sub_parser.add_argument("divisions", type=projecteuler.unsigned_int, help="maximum number of divisions allowed")
    sub_parser.add_argument(
        "dimensions", nargs="?", default=3, type=projecteuler.unsigned_int, help="dimensionality of the space grid"
    )
    sub_parser.add_argument("-i", "--images", action="store_true", help="store amoebas colony to disc as images")
    sub_parser.add_argument("-a", "--async", dest="async_", action="store_true", help="run asynchronously")
    sub_parser.set_defaults(main_function=main)
