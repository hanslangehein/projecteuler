"""
Amoebas: Class for growing an amoebas colony.

Copyright (C) 2024  Hans Langehein

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import asyncio
import itertools
import logging
import typing

from projecteuler.problems.p763.memory import MemoryParent

logger = logging.getLogger(__name__)


class Amoebas:
    """Class for growing an amoebas colony."""

    COLLISION_MSG = "After %s divisions: Collision detected, did not evolve %s. (of %s) amoeba with coordinates %s"
    EXISTS_MSG = "After %s divisions: Arrangement exists, did not evolve %s. (of %s) amoeba with coordinates %s"
    MAX_DEPTH_MSG = "After %s divisions: Maximum exceeded, did not evolve %s. (of %s) amoeba with coordinates %s"
    EVOLVED_MSG = "After %s divisions: Evolved %s. (of %s) amoeba with coordinates %s"

    __memory: MemoryParent

    def __init__(self, memory: MemoryParent) -> None:
        """"""
        self.__memory = memory

    @property
    def n_arrangements(self) -> tuple[int, ...]:
        """"""
        return self.__memory.n_arrangements

    def recurse(self, arrangement: set[tuple[int, ...]], n_divisions: int) -> None:
        """Given an arrangement of amoebas, recursively update memory for each possible child arrangement."""

        n_amoebas = len(arrangement)
        logger.debug("After %s divisions: Recursing arrangement holding %s amoebas", n_divisions, n_amoebas)

        n_divisions += 1

        for index, amoeba in enumerate(arrangement):
            info_tuple = (n_divisions, index + 1, n_amoebas, amoeba)

            if self.__memory.max_reached(n_divisions):
                logger.debug(self.MAX_DEPTH_MSG, *info_tuple)
                continue

            child_amoebas = self.evolve(amoeba)

            if child_amoebas & arrangement:
                logger.debug(self.COLLISION_MSG, *info_tuple)
                continue

            child_arrangement = child_amoebas | (arrangement - {amoeba})

            if self.__memory.exists(self.permute(child_arrangement), n_divisions):
                logger.debug(self.EXISTS_MSG, *info_tuple)
                continue

            logger.debug(self.EVOLVED_MSG, *info_tuple)
            self.recurse(child_arrangement, n_divisions)

    async def recurse_async(self, arrangement: set[tuple[int, ...]], n_divisions: int) -> None:
        """Given an arrangement of amoebas, recursively update memory for each possible child arrangement."""

        n_amoebas = len(arrangement)
        logger.debug("After %s divisions: Recursing arrangement holding %s amoebas", n_divisions, n_amoebas)

        n_divisions += 1

        tasks = set()
        for index, amoeba in enumerate(arrangement):
            info_tuple = (n_divisions, index + 1, n_amoebas, amoeba)

            if self.__memory.max_reached(n_divisions):
                logger.debug(self.MAX_DEPTH_MSG, *info_tuple)
                continue

            child_amoebas = self.evolve(amoeba)

            if child_amoebas & arrangement:
                logger.debug(self.COLLISION_MSG, *info_tuple)
                continue

            child_arrangement = child_amoebas | (arrangement - {amoeba})

            if await self.__memory.exists_async(self.permute(child_arrangement), n_divisions):
                logger.debug(self.EXISTS_MSG, *info_tuple)
                continue

            logger.debug(self.EVOLVED_MSG, *info_tuple)

            # Create a task and add to set
            task = asyncio.create_task(self.recurse_async(child_arrangement, n_divisions))
            tasks.add(task)

            # Make each task remove its own reference after completion
            task.add_done_callback(tasks.discard)

        # Schedule tasks concurrently
        await asyncio.gather(*tasks)

        # Suspend the current task, allowing other tasks to run
        await asyncio.sleep(0)

    @classmethod
    def evolve(cls, amoeba: tuple[int, ...]) -> set[tuple[int, ...]]:
        """
        Given one amoeba (*n*D coordinate), create *n* child amoebas, where *n* is the number of dimensions.

        In a 2D space grid, this method is equivalent to:

        ```
        x, y = amoeba

        return {(x + 1, y), (x, y + 1)}
        ```

        In a 3D space grid, this method is equivalent to:

        ```
        x, y, z = amoeba

        return {(x + 1, y, z), (x, y + 1, z), (x, y, z + 1)}
        ```
        """

        n_dims = len(amoeba)

        child_amoebas = set()
        for axis in range(n_dims):
            child_amoeba = list(amoeba)
            child_amoeba[axis] += 1
            child_amoebas.add(tuple(child_amoeba))

        return child_amoebas

    @classmethod
    def permute(cls, arrangement: set[tuple[int, ...]]) -> typing.Iterator[set[tuple[int, ...]]]:
        """
        Given an arrangement of amoebas, yield arrangements with every possible coordinate permutation.

        In a 2D space grid, the original arrangement and a mirrored version is returned.

        The following arrangements are created in a 3D space grid:

        - `(x, y, z)` -> Identical arrangement
        - `(x, z, y)` -> Mirrored arrangement
        - `(y, x, z)` -> Mirrored arrangement
        - `(y, z, x)` -> Rotated arrangement
        - `(z, x, y)` -> Rotated arrangement
        - `(z, y, x)` -> Mirrored arrangement

        This is roughly equivalent to:

        ```
        # Access first amoeba to find out the dimension of space
        n_dims = len(tuple(arrangement)[0])

        # If n_dims=2, basis will be either (0, 1) or (1, 0)
        for vector_basis in itertools.permutations(range(n_dims)):

            # Initialize new arrangement to be filled with permuted amoebas
            permuted_arrangement = set()

            # Loop through old arrangement to access each old amoeba
            for amoeba in arrangement:

                # Initialize new amoeba to be filled with permuted coordinates
                permuted_amoeba = []

                # Loop through the basis, axis will be anything between 0 and n_dims-1
                for axis in vector_basis:

                    # In an order depending on the basis, access each coordinate of old amoeba
                    coord = amoeba[axis]

                    # Update new amoeba
                    permuted_amoeba.append(coord)

                # Update new arrangement
                permuted_arrangement.add(tuple(permuted_amoeba))

            # Yield results
            yield permuted_arrangement
        ```
        """

        n_dims = len(next(iter(arrangement))) if len(arrangement) else 0

        yield from (
            {tuple(amoeba[axis] for axis in vector_basis) for amoeba in arrangement}
            for vector_basis in itertools.permutations(range(n_dims))
        )
