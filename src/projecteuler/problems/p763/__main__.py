"""
P763: Amoebas in a 3D Grid.

See <https://projecteuler.net/problem=763> for problem statement.

Copyright (C) 2024  Hans Langehein

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import logging

import projecteuler

logger = logging.getLogger(__package__)


sub_parser = argparse.ArgumentParser(parents=(projecteuler.get_parent_parser(),))
projecteuler.problems.p763.parser_setup(sub_parser)

args = sub_parser.parse_args()
projecteuler.logging_setup(args)
result = args.main_function(args)

logger.info("Result: %s", result)
