"""
Memory: Classes for storing an amoebas colony.

Copyright (C) 2024  Hans Langehein

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import abc
import asyncio
import logging
import pathlib
import typing

import PIL.Image

logger = logging.getLogger(__name__)


class MemoryParent(abc.ABC):
    """"""

    __max_depth: int
    __hashes: list[set[int]]
    __locks: tuple[asyncio.Lock, ...]

    def __init__(self, max_depth: int) -> None:
        """"""
        self.__max_depth = max_depth
        self.__hashes = [set() for _ in range(max_depth)]
        self.__locks = tuple(asyncio.Lock() for _ in range(max_depth))

    @property
    def n_arrangements(self) -> tuple[int, ...]:
        """"""
        return tuple(len(hash_) for hash_ in self.__hashes)

    def max_reached(self, depth: int) -> bool:
        """"""
        return depth > self.__max_depth

    def lock(self, depth: int) -> asyncio.Lock:
        """"""
        return self.__locks[depth - 1]

    def exists(self, arrangements: typing.Iterator[set[tuple[int, ...]]], depth: int) -> bool:
        """Check if the hashes of given arrangements already exist and add to memory, if not."""

        if self.max_reached(depth):
            msg = f"Maximum depth reached ({depth} > {self.__max_depth})"
            logger.warning(msg)
            raise MemoryError(msg)

        first_arrangement = next(arrangements)

        first_hash = {self.hash_arrangement(first_arrangement)}

        if first_hash & self.__hashes[depth - 1]:
            return True

        new_hashes = first_hash | {self.hash_arrangement(arrangement) for arrangement in arrangements}
        self.__hashes[depth - 1] |= new_hashes

        self._save(first_arrangement, depth)

        return False

    async def exists_async(self, arrangements: typing.Iterator[set[tuple[int, ...]]], depth: int) -> bool:
        """Check if the hashes of given arrangements already exist and add to memory, if not."""

        if self.max_reached(depth):
            msg = f"Maximum depth reached ({depth} > {self.__max_depth})"
            logger.warning(msg)
            raise MemoryError(msg)

        first_arrangement = next(arrangements)

        first_hash = {self.hash_arrangement(first_arrangement)}

        if first_hash & self.__hashes[depth - 1]:
            return True

        new_hashes = first_hash | {self.hash_arrangement(arrangement) for arrangement in arrangements}
        self.__hashes[depth - 1] |= new_hashes

        async with self.lock(depth):
            await asyncio.to_thread(self._save, first_arrangement, depth)

        return False

    @classmethod
    def hash_arrangement(cls, arrangement: set[tuple[int, ...]]) -> int:
        """"""
        return hash(tuple(sorted(arrangement)))

    @abc.abstractmethod
    def _save(self, arrangement: set[tuple[int, ...]], depth: int) -> None:
        """"""


class Memory(MemoryParent):
    """Class for storing the hashes of an amoebas colony's arrangements."""

    def _save(self, arrangement: set[tuple[int, ...]], depth: int) -> None:
        """"""


class ImageMemory(MemoryParent):
    """Class for storing an amoebas colony's arrangements to disc as images."""

    DIRNAME = pathlib.Path("_amoebas")

    __filepaths: tuple[pathlib.Path, ...]

    def __init__(self, max_depth: int) -> None:
        """"""

        super().__init__(max_depth)

        self.DIRNAME.mkdir(exist_ok=True)

        self.__filepaths = tuple(self.DIRNAME.joinpath(f"{depth:0{9}}.gif") for depth in range(1, max_depth + 1))

        for depth in range(1, max_depth + 1):
            img = self.init_frame(depth)
            self.save_frames([img], self.filepath(depth))

    def filepath(self, depth: int) -> pathlib.Path:
        """Path to the image file for a given depth."""

        return self.__filepaths[depth - 1]

    @property
    def file_sizes(self) -> typing.Iterator[int]:
        """Size of each image file saved to disc in bytes."""

        yield from (filepath.stat().st_size for filepath in self.__filepaths if filepath.is_file())

    @classmethod
    def img_size(cls, depth: int) -> tuple[int, int]:
        """Size of an image as 2-tuple, containing width and height in pixels."""

        return (depth + 1, depth + 1)

    @classmethod
    def init_frame(cls, depth: int) -> PIL.Image.Image:
        """"""
        return PIL.Image.new(mode="1", size=cls.img_size(depth))

    @classmethod
    def save_frames(cls, imgs: typing.Sequence[PIL.Image.Image], filepath: pathlib.Path) -> None:
        """"""

        img = imgs[0]
        img.save(filepath, append_images=imgs, save_all=True, loop=0, duration=1, optimize=False)

    @classmethod
    def load_frames(cls, filepath: pathlib.Path) -> typing.Iterator[PIL.Image.Image]:
        """"""

        img = PIL.Image.open(filepath)

        try:
            while True:
                yield img.copy()
                img.seek(img.tell() + 1)
        except EOFError:
            pass

    @classmethod
    def create_frame(cls, arrangement: set[tuple[int, ...]], depth: int) -> PIL.Image.Image:
        """"""

        img = cls.init_frame(depth)
        for amoeba in arrangement:
            pixel = [0, 0]
            pixel[: len(amoeba)] = amoeba[:2]
            img.putpixel(typing.cast(tuple[int, int], tuple(pixel)), 255)

        return img

    def _save(self, arrangement: set[tuple[int, ...]], depth: int) -> None:
        """"""

        frames = list(self.load_frames(self.filepath(depth)))

        new_frame = self.create_frame(arrangement, depth)

        frames.append(new_frame)

        self.save_frames(frames, self.filepath(depth))

        logger.debug("Current sizes in bytes (total: %s): %s", sum(sizes := tuple(self.file_sizes)), sizes)
