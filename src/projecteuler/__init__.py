"""
ProjectEuler: Solutions to Project Euler problems.

See <https://projecteuler.net/about> to learn more.

Copyright (C) 2024  Hans Langehein

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from projecteuler import problems

__all__ = ["problems"]

import argparse
import importlib.metadata
import logging
import time

import projecteuler

_METADATA = importlib.metadata.metadata("projecteuler")
__author__ = _METADATA["Author"]
__version__ = _METADATA["Version"]

logger = logging.getLogger(__package__)


def main() -> None:
    """"""

    main_parser = argparse.ArgumentParser(
        prog=__package__, description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    main_parser.add_argument("-v", "--version", action="version", version=__version__)
    sub_parsers = main_parser.add_subparsers(
        required=True, help="numeric identifier of the problem (choices: %(choices)s)", metavar="id"
    )

    sub_parser = sub_parsers.add_parser("1", parents=(get_parent_parser(),))
    projecteuler.problems.p1.parser_setup(sub_parser)

    sub_parser = sub_parsers.add_parser("762", parents=(get_parent_parser(),))
    projecteuler.problems.p762.parser_setup(sub_parser)

    sub_parser = sub_parsers.add_parser("763", parents=(get_parent_parser(),))
    projecteuler.problems.p763.parser_setup(sub_parser)

    args = main_parser.parse_args()
    logging_setup(args)

    time_now = time.time()
    result = args.main_function(args)
    time_diff = time.time() - time_now

    logger.info("Result after %.3f s: %s", time_diff, result)


def get_parent_parser() -> argparse.ArgumentParser:
    """"""

    parent_parser = argparse.ArgumentParser(add_help=False)
    parent_parser.add_argument(
        "-l",
        "--console-logging",
        default="info",
        choices=("debug", "info", "warning", "error", "critical"),
        metavar="level",
        help="set console logging level using textual representation (default: %(default)s, choices: %(choices)s)",
    )
    parent_parser.add_argument(
        "-f",
        "--file-logging",
        default="info",
        choices=("debug", "info", "warning", "error", "critical"),
        metavar="level",
        help="set file logging level using textual representation (default: %(default)s, choices: %(choices)s)",
    )

    return parent_parser


def logging_setup(args: argparse.Namespace) -> None:
    """"""

    logger.propagate = False
    logger.setLevel(-1)

    filename = f"_{logger.name}.log"
    formatter = logging.Formatter("%(levelname)8s|%(asctime)s|%(name)s: %(message)s", datefmt="%H:%M:%S")

    levels: tuple[str, ...] = (args.console_logging.upper(), args.file_logging.upper())
    handlers: tuple[logging.Handler, ...] = (logging.StreamHandler(), logging.FileHandler(filename, "w", "utf-8"))

    for level, handler in zip(levels, handlers, strict=True):
        handler.setFormatter(formatter)
        handler.setLevel(level)
        logger.addHandler(handler)

    logger.debug("Command-line arguments:")
    for arg in vars(args):
        logger.debug("  %s = %s", arg, getattr(args, arg))


def unsigned_int(string: str) -> int:
    """"""

    msg = f"invalid positive int value: {string!r}"
    try:
        value = int(string)
    except ValueError as exc:
        raise argparse.ArgumentTypeError(msg) from exc
    if value < 0:
        raise argparse.ArgumentTypeError(msg)
    return value
